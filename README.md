# DevOps Experiance - Network Policies - 26/08/2021

## Docker isolatio in action

1. Create two containers and check they cannot see each other
Then run them in the same PID namespace

```sh
docker run --name c-1 -d ubuntu sh -c 'sleep 1d'
docker run --name c-2 -d ubuntu sh -c 'sleep 999d'
docker exec c-1 ps aux
docker exec c-2 ps aux
ps aux | grep sleep
docker rm c-2 --force
docker run --name c-2 --pid=container:c-1 -d ubuntu sh -c 'sleep 999d'
docker exec c-2 ps aux
```

## Create pod resources and test default policies

```sh
kubectl run frontend --image=nginx
kubectl run backend --image=nginx
kubectl expose pod frontend --port=80 
kubectl expose pod backend --port=80 
kubectl get po,svc
kubectl exec frontend -- curl backend
kubectl exec backend -- curl frontend
```

## Default Deny

1. Create Default Deny networkPolicy

```sh
vim default-deny.yaml
```

```yaml
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: default-deny
  namespace: default
spec:
  podSelector: {}
  policyTypes:
  - Ingress
  - Egress
```

```sh
kubectl apply -f default-deny.yaml
```

# Allow based on podSelector

## NetworkPolicy podSelector allow Egress from Frontend to Backent

2. Allow frontend pods to talk to backend pods

```sh
vim frontend-policy.yaml
```

```yaml
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: frontend-policy
  namespace: default
spec:
  podSelector:
    matchLabels:
      run: frontend
  policyTypes:
  - Egress
  egress:
  - to:
    - podSelector:
        matchLabels:
          run: backend
```

```sh
kubectl apply -f frontend-policy.yaml
```

## NetworkPolicy podSelecto allow Ingress from Frontend to Backend

```sh
vim backend-policy.yaml
```

```yaml
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: backend-policy
  namespace: default
spec:
  podSelector:
    matchLabels:
      run: backend
  policyTypes:
  - Ingress
  ingress:
  - from:
    - podSelector:
        matchLabels:
          run: frontend
```

```sh
kubectl apply -f backend-policy.yaml
kubectl get pod --show-labels -o wide
kubectl exec frontend -- curl ${backend-ip}
```

# Issue DNS 

## Deny all incoming and outgoing traffic from all pods in namespace default
## but allow DNS traffic.

```sh
vim default-deny.yaml
```

```yaml
apiVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
  name: deny
  namespace: default
spec:
  podSelector: {}
  policyTypes:
  - Egress
  - Ingress
  egress:
  - ports:
    - port: 53
      protocol: TCP
    - port: 53
      protocol: UDP
```

```sh
kubectl apply -f default-deny.yaml
kubectl exec frontend -- curl backend
```
